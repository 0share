# Copyright (C) 2008, Thomas Leonard
# See the README file for details, or visit http://0install.net.

from logging import info, debug
import socket, sys, select, tempfile, shutil
import httplib

from zeroinstall.zerostore import Stores, NotStored

port = 38339

stores = Stores()

def fetch(host, digests):
	if not digests:
		print >>sys.stderr, "No digests given!"
		sys.exit(1)

	client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)

	if host is None:
		host = '<broadcast>'
	if host == '<broadcast>':
		info("Broadcasting query for %s on local network...", digests)
		client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	else:
		info("Sending query for %s to %s...", digests, host)

	client.sendto('0share\n' + '\n'.join(digests), (host or '<broadcast>', port))

	needed = set(digests)

	n_responses = 0
	while needed:
		r = select.select([client], [], [], 1)[0]
		if r:
			data, addr = client.recvfrom(128)
			addr = addr[0]
			lines = data.split('\n')
			if lines[0] == '0share-reply':
				n_responses += 1
				for d in lines[1:]:
					print "%s has %s" % (addr, d)
					if d in needed:
						fetch_impl(d, addr)
						needed.remove(d)
					else:
						info("Don't need %s now", d)
			else:
				info("Invalid reply from %s: %s", addr, repr(data))
		else:
			break
	if needed:
		if n_responses:
			info("No further responses for one second")
		else:
			info("No responses within one second")
		for x in needed:
			print "Failed to fetch", x
	else:
		print "Success"

def fetch_impl(digest, addr):
	try:
		path = stores.lookup(digest)
		info("Already have %s - not fetching", path)
		return
	except NotStored:
		pass

	info("Connecting to %s to request %s", addr, digest)
	client = httplib.HTTPConnection(addr, port, strict = True)
	url = '/implementation/' + digest
	client.request('GET', url)
	response = client.getresponse()
	type = response.getheader('Content-Type')
	if not type:
		raise Exception("Missing Content-Type in response")

	if response.status != 200:
		raise Exception("Error fetching implementation (%d): %s" % (response.status, response.reason))

	tmp = tempfile.TemporaryFile(prefix = '0share-')
	try:
		shutil.copyfileobj(response, tmp)
		tmp.seek(0)
		stores.add_archive_to_cache(digest, tmp, url, type = type)
	finally:
		tmp.close()
	client.close()
